# sudo apt install python3-pyotp python3-qrcode python3-requests
# python3 ./file.py [duo_qrcode_url|qrcode_data]
import qrcode
import pyotp
import requests
import base64
import sys
import re

if len(sys.argv) < 2:
    print('copy the duo qrcode image url, or the raw qrcode data')
    data = sys.stdin.readline().replace('\n', '').replace('\r', '')
else:
    data = sys.argv[1]

g = re.match('https:\/\/.*\?value=(.*)', data)
if g:
    data = g.group(1)
if not len(data.split('-')) == 2:
    raise 'not valid url or qrcode'

hostb64 = data.split('-')[1]
code = data.split('-')[0]
host = base64.b64decode(hostb64 + '='*(-len(hostb64) % 4))
host = host.decode()
url = 'https://{0}/push/v2/activation/{1}'.format(host, code)
r = requests.post(url)
print("return", r)
print("return_json", r.json())
hotp_secret = r.json()['response']['hotp_secret']
secret = base64.b32encode(hotp_secret.encode()).decode()
print("HOTP secret base64.b32encode", secret)
hotp = pyotp.HOTP(secret)
print('First 5 hotp codes')
for i in range(5):
    print(hotp.at(i))
otpauth_uri = 'otpauth://hotp/Duo:Duo@?issuer=duo&secret={0}'.format(requests.utils.quote(secret))
print(otpauth_uri)
qr = qrcode.QRCode()
qr.add_data(otpauth_uri)
qr.print_ascii(tty=True)
