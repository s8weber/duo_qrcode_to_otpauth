Usage
---------

 1. In duo device managment add a new device (ipad ?)
 2. When it shows a QRcode grab its URL.
 3. Launch the python script with the URL or raw qrcode
 4. If everything goes to plan your terminal should display a qrcode, and the secret
 5. You can scan the qrcode with an app like FreeOTP or andOTP or perhaps use the HOTP secret


Install
-------

```
sudo apt install python3-pyotp python3-qrcode python3-requests
```

Example
-----------

```
python3 ./duo_qrcode_to_otpauth.py UFtVQRIohaXXXXXXXXXX-YXBpLTRXXXXXXXXXXXXXXXX


return <Response [200]>
return_json {'response': {'akey': 'XXXXXXXXXX', 'customer_name': 'XXXXX',
'has_backup_restore': True, 'has_bluetooth_approve': False,
'has_trusted_endpoints': False, 'hotp_secret': 'XXXX', 'is_fips_deployment': False,
'pkey': 'XXXXXX', 'reactivation_token': 'XXXX', 'requires_fips_android': False,
'requires_mdm': 0, 'security_checkup_enabled': False, 'urg_secret': 'XXXX'},
stat': 'OK'}
HOTP secret MRSTANZVMZRTXXXXXXXXXXXXXXXXXXXX
310423
279261
228353
076492
559126
otpauth://hotp/Duo:Duo@?issuer=duo&secret=MRSTANXXXXXXXXXXXX
█████████████████████████████████████████████
█████████████████████████████████████████████
████ ▄▄▄▄▄ █▄██▀██ ▀█▄█▀ ▄ ▄▀▄▀▀▄█ ▄▄▄▄▄ ████
████ █   █ █▄ ▀▄  ▄▀ █▄   ▄    ▀██ █   █ ████
████ █▄▄▄█ █▀ ████▀▀▄█▄█▄█▀█▀██▄▀█ █▄▄▄█ ████
████▄▄▄▄▄▄▄█▄▀ █▄▀▄█ ▀ ▀▄█ █ ▀ ▀ █▄▄▄▄▄▄▄████
████ ▀▄█▀▀▄▄▀▄▄█▀▀█▄▄▀ ▀█▀█▄▄▄ █▄▀█▄██▄▀ ████
████ ▀ █ █▄▀ ▄▀ ▀▄ ▀▀▄ █ ▄▀▀▀▄▀▄ ▀▀▄█▄▄█▄████
████▀▀▀ ▄ ▄  █▄██ ▀▄█ ▀▄ █▄  ▀█▀▄▄ ▄█▄▄█▄████
████▄▀█▄██▄▀  ▄█ ▄███▀▄▄▄▀▄ ▄▀ ▄▄█▄▄▀ ▄▀█████

```

